/* @file Drupal Ipsum generation node form script */

jQuery(document).ready(function($){
  // tab behavior
	$('ul.tabs.primary').find('a').each(function(){
    var $t = $(this);

    if ($t.text() === Drupal.t('Drupal Ipsum')) {
      $t.click(function(e){
        $.getJSON($t.attr('href') + '?js=true', function(response){
          if (response.ok) {
            $('textarea[id^=edit-body]').html(response.data);
          }
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
      });
      return;
    }
  });
  
  // checkbox
  $('#edit-drupal-ipsum-fill').bind('change', function(){    
    if ($(this).is(':checked')) {
      $('#edit-body').slideUp();
    } else {
      $('#edit-body').slideDown();
    }
  });
});