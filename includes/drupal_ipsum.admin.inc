<?php
/**
 * @file Drupal Ipsum admin pages
 */

/**
 * Form callback
 * 
 * @see drupal_ipsum_menu().
 */
function _drupal_ipsum_admin_config() {
  // get base form
  $form = _drupal_ipsum_get_form();
  
  // add markup element
  $form['markup'] = array(
    '#markup' => '<h3>' . t('Default settings for Drupal Ipsum') . '</h3>',
    '#weight' => -999,
  );
  
  return system_settings_form($form);
}

/**
 * Validate callback
 * 
 * @see _drupal_ipsum_admin_config().
 */
function _drupal_ipsum_admin_config_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['drupal_ipsum_length'])) {
    form_set_error('drupal_ipsum_length', t('Drupal Ipsum text length must be numeric'));
  }
}